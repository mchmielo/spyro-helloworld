import { Grid } from '@material-ui/core';
import React from 'react';
import { useCartContext } from 'services';
import { ProductAvatar } from '../ProductAvatar';
import { CartItemControls } from './components';

interface CartProps {
    readonly isReadOnly: boolean;
}

export function Cart({ isReadOnly }: CartProps) {
    const { cart } = useCartContext();

    return (
        <ul>
            {cart.map(({ product: { id, name }, amount }) => (
                <li key={id}>
                    <Grid container>
                        <Grid item>
                            <ProductAvatar productId={id} />
                        </Grid>
                        <Grid item>
                            {name} - <CartItemControls productId={id} amount={amount} isReadOnly={isReadOnly} />
                        </Grid>
                    </Grid>
                </li>
            ))}
        </ul>
    );
}
