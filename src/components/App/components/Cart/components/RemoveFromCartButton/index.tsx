import { IconButton } from '@material-ui/core';
import React, { useCallback } from 'react';
import { ProductId } from 'services/interfaces';
import { RemoveShoppingCart } from '@material-ui/icons';
import { useCartContext } from 'services';

interface RemoveFromCartButtonProps {
    readonly productId: ProductId;
    readonly className?: string;
}

export function RemoveFromCartButton({ productId, className }: RemoveFromCartButtonProps) {
    const { removeFromCart, isInCart } = useCartContext();

    const handleRemoveFromCartClick = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            removeFromCart(productId);
        },
        [productId, removeFromCart],
    );

    return (
        <IconButton
            color="primary"
            className={className}
            onClick={handleRemoveFromCartClick}
            disabled={!isInCart(productId)}
        >
            <RemoveShoppingCart />
        </IconButton>
    );
}
