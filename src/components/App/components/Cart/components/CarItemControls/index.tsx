import { IconButton, makeStyles, TextField } from '@material-ui/core';
import { Add, Remove } from '@material-ui/icons';
import React, { useCallback } from 'react';
import { useCartContext } from 'services';
import { ProductId } from 'services/interfaces';
import { RemoveFromCartButton } from '../RemoveFromCartButton';

interface CartItemControlsProps {
    readonly productId: ProductId;
    readonly amount: number;
    readonly isReadOnly: boolean;
}

export function CartItemControls({ productId, amount, isReadOnly }: CartItemControlsProps) {
    const classes = useStyles();

    const { increment, decrement, setAmount } = useCartContext();

    const handleSubstractClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            decrement(productId);
        },
        [productId, decrement],
    );

    const handleAddClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            increment(productId);
        },
        [productId, increment],
    );

    const handleInputChange = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            setAmount(productId, Number.parseInt(e.currentTarget.value));
        },
        [productId, setAmount],
    );

    if (!isReadOnly) {
        return (
            <>
                <IconButton onClick={handleSubstractClick}>
                    <Remove />
                </IconButton>
                <TextField
                    value={amount}
                    onChange={handleInputChange}
                    className={classes.input}
                    inputProps={{ style: { textAlign: 'center' } }}
                />
                <IconButton onClick={handleAddClick}>
                    <Add />
                </IconButton>
                <RemoveFromCartButton productId={productId} />
            </>
        );
    }
    return <TextField disabled={true} value={amount}></TextField>;
}

const useStyles = makeStyles(() => ({
    input: {
        width: 100,
    },
}));
