import { Avatar, CircularProgress } from '@material-ui/core';
import { BrokenImage } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import { useProducts } from 'services';
import { Product, ProductId } from 'services/interfaces';

interface ProductAvatarProps {
    readonly productId: ProductId;
    readonly className?: string;
}

export function ProductAvatar({ productId, className }: ProductAvatarProps) {
    const { getProduct } = useProducts();

    const [product, setProduct] = useState<undefined | null | Product>(undefined);

    useEffect(() => {
        getProduct(productId).then(setProduct, setProduct.bind(undefined, null));
    }, [getProduct, productId]);

    if (product === undefined) {
        return <CircularProgress />;
    }

    return (
        <Avatar
            src={product ? product.miniature : undefined}
            alt={product ? product.shortDescription : ''}
            className={className}
        >
            <BrokenImage />
        </Avatar>
    );
}
