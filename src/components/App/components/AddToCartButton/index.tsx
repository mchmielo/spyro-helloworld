import { Button } from '@material-ui/core';
import React, { useCallback } from 'react';
import { ProductId } from 'services/interfaces';
import { AddShoppingCart } from '@material-ui/icons';
import { useCartContext } from 'services';

interface AddToCartButtonProps {
    readonly productId: ProductId;
    readonly className?: string;
}

export function AddToCartButton({ productId, className }: AddToCartButtonProps) {
    const { addToCart, isInCart } = useCartContext();

    const handleAddToCartClick = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();
            addToCart(productId);
        },
        [productId, addToCart],
    );

    return (
        <Button
            variant="contained"
            color="primary"
            className={className}
            onClick={handleAddToCartClick}
            disabled={isInCart(productId)}
            startIcon={<AddShoppingCart />}
        >
            Add to Cart
        </Button>
    );
}
