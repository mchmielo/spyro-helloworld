import React, { PropsWithChildren } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AuthContextProvider, CartContextProvider, ProductContextProvider } from 'services';

type AppContextsProps = PropsWithChildren<Record<string, any>>;

export function AppContexts({ children }: AppContextsProps) {
    return (
        <BrowserRouter>
            <AuthContextProvider>
                <ProductContextProvider>
                    <CartContextProvider>{children}</CartContextProvider>
                </ProductContextProvider>
            </AuthContextProvider>
        </BrowserRouter>
    );
}
