import { CartPage, LoginPage, OrderAddressPage, OrderConfirmationPage, ProductListPage, ProductPage } from 'pages';
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { PrivateRoute } from '..';

export function AppRoutes() {
    return (
        <Switch>
            <Route exact path="/" component={ProductListPage} />
            <Route exact path="/login" component={LoginPage} />
            <PrivateRoute path="/cart">
                <CartPage />
            </PrivateRoute>
            <PrivateRoute path="/order-address">
                <OrderAddressPage />
            </PrivateRoute>
            <PrivateRoute path="/order-confirmation">
                <OrderConfirmationPage />
            </PrivateRoute>
            <Route path="/product/:productId" component={ProductPage} />
            <Redirect to="/" from="/products" />
        </Switch>
    );
}
