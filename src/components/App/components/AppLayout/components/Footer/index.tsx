import { Link, makeStyles, Typography } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
    footer: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
}));

export function Footer() {
    const classes = useStyles();

    return (
        <footer className={classes.footer}>
            <Typography variant="body2" color="textSecondary" align="center">
                {'Copyleft © '}
                <Link color="inherit" href="https://google.com">
                    google
                </Link>
                {new Date().getFullYear()}
                {'.'}
            </Typography>
        </footer>
    );
}
