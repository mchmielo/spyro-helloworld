import { AppBar, Button, IconButton, Toolbar, Typography, MenuItem } from '@material-ui/core';
import React, { useCallback } from 'react';
import { Menu as MenuIcon } from '@material-ui/icons';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { UserMenu } from './components/UserMenu';
import { useHistory, Link } from 'react-router-dom';
import { useAuthContext } from 'services/AuthContext';
import { CartMenuButton } from './components';

export function TopBar() {
    const history = useHistory();

    const { user } = useAuthContext();

    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            menuButton: {
                marginRight: theme.spacing(2),
            },
            title: {
                flexGrow: 1,
            },
            link: {
                color: 'inherit',
                textDecoration: 'none',
            },
        }),
    );

    const classes = useStyles();

    const handleMenuClicked = useCallback(() => {
        console.log('Menu clicked');
    }, []);

    const handleLoginButtonClick = useCallback(() => {
        history.push('/login');
    }, [history]);

    const handleCartButtonClick = useCallback(() => {
        history.push('/cart');
    }, [history]);

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton
                    edge="start"
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="menu"
                    onClick={handleMenuClicked}
                >
                    <MenuIcon />
                </IconButton>

                <Typography variant="h6" className={classes.title}>
                    <Link to="/" className={classes.link}>
                        Tutorial
                    </Link>
                </Typography>

                {user !== null && (
                    <MenuItem>
                        <CartMenuButton onClick={handleCartButtonClick} />
                    </MenuItem>
                )}

                {user === null && (
                    <Button color="inherit" onClick={handleLoginButtonClick}>
                        Login
                    </Button>
                )}
                {user !== null && <UserMenu />}
            </Toolbar>
        </AppBar>
    );
}
