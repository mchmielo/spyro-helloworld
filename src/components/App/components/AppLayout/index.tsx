import { Container, CssBaseline, makeStyles } from '@material-ui/core';
import { TopBar } from 'components/App/components/AppLayout/components/TopBar';
import React from 'react';
import { Footer } from './components';

type AppLayoutProps = React.PropsWithChildren<Record<string, any>>;

export function AppLayout({ children }: AppLayoutProps) {
    const classes = useStyles();

    return (
        <>
            <TopBar />
            <Container maxWidth="lg" component="main" className={classes.main}>
                <CssBaseline />
                {children}
            </Container>
            <Footer />
        </>
    );
}

const useStyles = makeStyles((theme) => ({
    main: {
        flexGrow: 1,
        flexShrink: 0,
    },
}));
