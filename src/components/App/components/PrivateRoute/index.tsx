import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { useAuthContext } from 'services';

export function PrivateRoute({ children, ...rest }: RouteProps) {
    const { user } = useAuthContext();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                user !== null ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
}
