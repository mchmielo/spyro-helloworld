import React, { MouseEvent, useCallback } from 'react';

interface TextProps {
    readonly onLinkClicked: (e: MouseEvent<HTMLAnchorElement>) => void;
    readonly fileLocation?: string;
}

export function Text({ onLinkClicked, fileLocation = 'src/App.tsx' }: TextProps) {
    const onClick = useCallback<(e: MouseEvent<HTMLAnchorElement>) => void>(
        (e) => {
            console.log('Clicked inside component.');
            e.preventDefault();
            onLinkClicked(e);
        },
        [onLinkClicked],
    );

    return (
        <>
            <p>
                Edit <code>{`'${fileLocation}'`}</code> and save to reload.
            </p>
            <a
                onClick={onClick}
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
            >
                Learn React
            </a>
        </>
    );
}
