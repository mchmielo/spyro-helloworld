import React from 'react';
import './App.css';

import 'services/firebase';
import { AppContexts, AppLayout, AppRoutes } from './components';

export function App() {
    return (
        <AppContexts>
            <AppLayout>
                <AppRoutes />
            </AppLayout>
        </AppContexts>
    );
}
