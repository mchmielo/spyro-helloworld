import { Button, Container, makeStyles, Paper, Typography } from '@material-ui/core';
import { Cart } from 'components/App/components';
import { toPairs } from 'lodash';
import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { orderService, useAuthContext, useCartContext } from 'services';

export function OrderConfirmationPage() {
    const classes = useStyles();
    const { cart, orderAddress, clearCart } = useCartContext();
    const { user } = useAuthContext();
    const history = useHistory();

    useEffect(() => {
        if (cart.length === 0 || orderAddress === undefined) {
            history.goBack();
        }
    }, [cart, orderAddress, history]);

    const handlePlaceOrderClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();

            if (user && orderAddress) {
                orderService
                    .placeOrder(user.uid, orderAddress, cart)
                    .then(() => history.replace('/'))
                    .then(clearCart);
            }

            console.log('Place order', orderAddress, cart);
        },
        [orderAddress, cart, user, clearCart, history],
    );

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Order confirmation
                </Typography>
            </Paper>
            <Paper className={classes.paper}>
                <Cart isReadOnly={true} />
            </Paper>
            <Paper className={classes.paper}>
                <ol>
                    {toPairs(orderAddress || {}).map(([key, value]) => (
                        <li key={key}>
                            {key}: {value}
                        </li>
                    ))}
                </ol>
            </Paper>
            <Paper className={classes.paper}>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={handlePlaceOrderClick}
                    disabled={!user || !orderAddress}
                >
                    Place order
                </Button>
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
}));
