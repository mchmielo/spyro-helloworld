import { Button, Container, makeStyles, Paper, Typography } from '@material-ui/core';
import { Cart } from 'components/App/components';
import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useCartContext } from 'services';

export function CartPage() {
    const classes = useStyles();
    const { cart } = useCartContext();

    const history = useHistory();

    const handleCheckoutClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();
            history.push('/order-address');
        },
        [history],
    );

    if (cart.length === 0) {
        return (
            <Paper className={classes.paper}>
                <Typography>No items in the cart.</Typography>
            </Paper>
        );
    }

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Cart
                </Typography>
            </Paper>
            <Paper>
                <Cart isReadOnly={false} />
            </Paper>
            {cart.length > 0 && (
                <Paper className={classes.paper}>
                    <Button size="large" variant="contained" color="primary" onClick={handleCheckoutClick}>
                        Checkout
                    </Button>
                </Paper>
            )}
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
}));
