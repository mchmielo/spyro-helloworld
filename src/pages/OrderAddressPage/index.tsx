import { Button, Container, Grid, makeStyles, Paper, TextField, TextFieldProps, Typography } from '@material-ui/core';
import React, { useCallback, useEffect } from 'react';
import { Field, FieldRenderProps, Form, FormRenderProps } from 'react-final-form';
import { useHistory } from 'react-router-dom';
import { useCartContext, formValidators } from 'services';
import { Address } from 'services/interfaces';
const initialValues: Address = {
    firstName: '',
    lastName: '',
    street: '',
    buildingNumber: '',
    flatNumber: '',
    city: '',
    postalCode: '',
};

export function OrderAddressPage() {
    const classes = useStyles();
    const { cart, setOrderAddress } = useCartContext();
    const history = useHistory();

    useEffect(() => {
        if (cart.length === 0) {
            history.goBack();
        }
    }, [cart, history]);

    const handleSubmit = useCallback(
        (values: Address) => {
            setOrderAddress(values);
            history.push('/order-confirmation');
        },
        [history, setOrderAddress],
    );

    const renderField = useCallback(
        (Component: React.ComponentType<TextFieldProps>) => ({
            input,
            meta: { touched, submitError, submitFailed, error },
            label,
        }: FieldRenderProps<Address[keyof Address]>) => {
            return (
                <Component
                    {...input}
                    label={label}
                    error={Boolean((touched || submitError || submitFailed) && error)}
                    helperText={(touched || submitError || submitFailed) && error ? error : undefined}
                />
            );
        },
        [],
    );

    const handleFormRender = useCallback(
        ({ handleSubmit, pristine, submitting }: FormRenderProps<Address>) => {
            return (
                <form onSubmit={handleSubmit} className={classes.form}>
                    <Grid container justify="center" alignContent="center">
                        <Grid item xs={12}>
                            <Field<Address['firstName']>
                                name="firstName"
                                label="First Name"
                                validate={formValidators.required()}
                                render={renderField(TextField)}
                            />
                            <Field<Address['lastName']>
                                name="lastName"
                                label="Last Name"
                                validate={formValidators.required()}
                                render={renderField(TextField)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field<Address['street']>
                                name="street"
                                label="Street"
                                validate={formValidators.required()}
                                render={renderField(TextField)}
                            />
                            <Field<Address['buildingNumber']>
                                name="buildingNumber"
                                label="Building Number"
                                validate={formValidators.required()}
                                render={renderField(TextField)}
                            />
                            <Field<Address['flatNumber']>
                                name="flatNumber"
                                label="Flat Number"
                                render={renderField(TextField)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field<Address['postalCode']>
                                name="postalCode"
                                label="Postal Code"
                                validate={formValidators.composeValidators(
                                    formValidators.required(),
                                    formValidators.postalCode(),
                                )}
                                render={renderField(TextField)}
                            />
                            <Field<Address['city']>
                                name="city"
                                label="City"
                                validate={formValidators.required()}
                                render={renderField(TextField)}
                            />
                        </Grid>
                    </Grid>

                    <Button color="primary" variant="contained" type="submit" disabled={pristine || submitting}>
                        Submit
                    </Button>
                </form>
            );
        },
        [classes.form, renderField],
    );

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Please provide your order address
                </Typography>
            </Paper>
            <Paper>
                <Form<Address> onSubmit={handleSubmit} render={handleFormRender} initialValues={initialValues}></Form>
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
}));
