import { Button, CircularProgress, Container, makeStyles, Paper, Typography } from '@material-ui/core';
import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useProducts } from 'services';
import { ProductId } from 'services/interfaces';
import { ProductCard } from './components';

export function ProductListPage() {
    const classes = useStyles();

    const history = useHistory();

    const { products, isLoading, fetchProducts } = useProducts();

    useEffect(fetchProducts, []);

    const handleShowProduct = useCallback(
        (id: ProductId) => {
            history.push(`/product/${id}`);
        },
        [history],
    );

    const handleClickLoadMore = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();
            fetchProducts();
        },
        [fetchProducts],
    );

    return (
        <Container maxWidth="lg">
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Our products
                </Typography>
                <Container className={classes.products}>
                    {products.length === 0 && <Typography>No Products</Typography>}
                    {products.length > 0 &&
                        products.map((product) => {
                            return (
                                <ProductCard
                                    {...product}
                                    key={product.id}
                                    className={classes.productCard}
                                    onShowProductClick={handleShowProduct}
                                />
                            );
                        })}
                    <div className={classes.progressContainer}>{isLoading && <CircularProgress />}</div>
                    <Button variant="contained" color="primary" disabled={isLoading} onClick={handleClickLoadMore}>
                        Load more...
                    </Button>
                </Container>
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    products: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly',
    },
    productCard: {
        margin: theme.spacing(2),
        maxWidth: '300px',
    },
    progressContainer: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
    },
}));
