import React, { useCallback } from 'react';
import { Product, ProductId } from 'services/interfaces';
import {
    Card,
    CardContent,
    CardHeader,
    CardMedia,
    makeStyles,
    Typography,
    CardActions,
    Button,
    Paper,
} from '@material-ui/core';
import { noop } from 'lodash';
import { AddToCartButton } from 'components/App/components';

type ProductCallback = (id: ProductId) => void;

interface ProductCardProps extends Product {
    className?: string;
    addToCard?: (productId: ProductId) => void;
    onShowProductClick?: ProductCallback;
}

export function ProductCard({
    className,
    id,
    name,
    price,
    shortDescription,
    onShowProductClick = noop,
    image,
}: ProductCardProps) {
    const classes = useStyles();

    const handleShowProductClick = useCallback(
        eventWrapper<HTMLButtonElement | HTMLDivElement>(id, onShowProductClick),
        [id, onShowProductClick],
    );

    return (
        <Card elevation={3} className={`${className}`}>
            <CardHeader title={name} />
            {!image ? (
                <Paper className={classes.emptyImage} />
            ) : (
                <CardMedia
                    image={image}
                    title={name}
                    className={classes.image}
                    onClick={handleShowProductClick}
                ></CardMedia>
            )}

            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {shortDescription}
                </Typography>
                <Typography>Price:{price.toFixed(2)}</Typography>
            </CardContent>
            <CardActions>
                <Button onClick={handleShowProductClick}>Show product</Button>
                <AddToCartButton productId={id} className={classes.addToCartButton}/>
            </CardActions>
        </Card>
    );
}

const useStyles = makeStyles((theme) => ({
    image: {
        height: '200px',
        cursor: 'pointer',
    },
    emptyImage: {
        height: '200px',
    },
    addToCartButton: {
        fontSize: '0.8em',
    },
}));

function eventWrapper<HTMLElement>(id: string, callback: ProductCallback) {
    return (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        e.stopPropagation();
        callback(id);
    };
}
