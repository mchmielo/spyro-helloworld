import { CircularProgress, Container, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import { AddToCartButton } from 'components/App/components/AddToCartButton';
import React, { useEffect, useState } from 'react';
import { Redirect, useParams } from 'react-router-dom';
import { fetchProduct } from 'services';
import { Product } from 'services/interfaces';

export function ProductPage() {
    const classes = useStyles();

    const { productId } = useParams();
    const [product, setProduct] = useState<Product | null | undefined>(undefined);

    useEffect(() => {
        fetchProduct(productId).then(setProduct);
    }, [setProduct, productId]);

    if (product === undefined) {
        return <CircularProgress />;
    }

    if (product === null) {
        return <Redirect to="/" />;
    }

    const { name, price, id, shortDescription, description, image } = product;

    return (
        <Container maxWidth="lg">
            <Paper className={classes.paper}>
                <Grid container spacing={2}>
                    <Grid item>
                        <div>
                            {image !== undefined && (
                                <img src={image} alt={shortDescription} width="300px" height="200px" />
                            )}
                        </div>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography component="h1" variant="h5" className={classes.title}>
                                    {name}
                                </Typography>
                                <Typography>{description}</Typography>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Typography>Price: {price.toFixed(2)}</Typography>
                            <AddToCartButton productId={id} className={classes.addToCartButton} />
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    products: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-evenly',
    },
    addToCartButton: {
        marginTop: theme.spacing(1),
    },
}));
