import { Product, ProductId } from 'services/interfaces';
import { collections } from '../firebase';
import firebase from 'firebase';
import { flow, partialRight, defaults, pick } from 'lodash';

const COLUMNS_TO_PICK_FROM_FIREBASE = ['id', 'name', 'price', 'shortDescription', 'description', 'miniature', 'image'];
const DEFAULT_VALUES = {
    id: '',
    name: 'Dafault name',
    price: '-1',
    shortDescription: '',
    description: '',
    miniature: undefined,
    image: undefined,
};

export function fetchProducts(
    productSnapshot: firebase.firestore.DocumentData | undefined | null,
    elementsOnPage = 10,
) {
    const collection = collections.products.orderBy('id');
    if (productSnapshot) {
        return collection.startAfter(productSnapshot).limit(elementsOnPage);
    }
    return collection.limit(elementsOnPage);
}

export function fetchProduct(productId: ProductId): Promise<Product | undefined> {
    const result = collections.products
        .where('id', '==', productId)
        .limit(1)
        .get()
        .then((data) => {
            if (data.docs.length === 0) {
                return undefined;
            }
            const element = data.docs[0].data();

            return {
                id: element.id,
                name: element.name,
                price: Number.parseFloat(element.price.toString()),
                shortDescription: element.shortDescription,
                description: element.description,
                miniature: element.miniature,
                image: element.image,
                available: element.available,
            };
        });
    return result;
}

export function getProducts(
    productSnapshot: firebase.firestore.DocumentData | undefined | null,
    elementsOnPage = 10,
): Promise<[firebase.firestore.DocumentData | undefined | null, Product[]]> {
    return fetchProducts(productSnapshot, elementsOnPage)
        .get()
        .then((data) => [data.docs[data.docs.length - 1], data.docs.map(parseProduct)]);
}

function parseProduct(element: firebase.firestore.QueryDocumentSnapshot<firebase.firestore.DocumentData>): Product {
    return flow([
        partialRight(pick, COLUMNS_TO_PICK_FROM_FIREBASE),
        partialRight(defaults, DEFAULT_VALUES),
        (e) => e as Product,
    ])({
        id: element.id,
        ...element.data(),
    });
}
