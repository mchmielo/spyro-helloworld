export const formValidators = Object.freeze({
    required: (message = 'This field is required') => (value: string | number | undefined) =>
        value ? undefined : message,
    postalCode: (message = 'Please use format XX-XXX') => (value: string | undefined = '') =>
        /^\d{2}-\d{3}$/.test(value) ? undefined : message,
    composeValidators: (...validators: Array<(value: string, allValues: any) => string | undefined>) => (
        value: string,
        allValues: any,
    ) => validators.reduce((error: string | undefined, validator) => error || validator(value, allValues), undefined),
});
