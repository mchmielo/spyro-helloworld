export type ProductId = string;
type ProductName = string;
type ProductPrice = number;
type ProductMiniatureURL = string;
type ProductImageURL = string;

export interface Product {
    readonly id: ProductId;
    readonly name: ProductName;
    readonly price: ProductPrice;
    readonly shortDescription: string;
    readonly description: string;
    readonly miniature?: ProductMiniatureURL;
    readonly image?: ProductImageURL;
    readonly available: boolean;
}

export interface Address {
    readonly firstName: string;
    readonly lastName: string;
    readonly street: string;
    readonly buildingNumber: string;
    readonly flatNumber: string;
    readonly postalCode: string;
    readonly city: string;
}

export interface FirebaseProduct {
    readonly id: string;
    readonly name: string;
    readonly price: string | number;
    readonly shortDescription: string;
    readonly description: string;
    readonly miniature?: string;
    readonly image?: string;
    readonly available: boolean;
    readonly dockey: string;
}

export interface FirebaseOrder extends Address {
    readonly userId: string;
    readonly orderedProducts: Array<{ productId: ProductId; amount: number }>;
}
