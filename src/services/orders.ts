import { Address, Product } from './interfaces';
import { collections } from './firebase';

export const orderService = Object.freeze({
    placeOrder(userId: string, address: Address, cart: Array<{ product: Product; amount: number }>) {
        return collections.orders.add({
            ...address,
            userId,
            orderedProducts: cart.map(({ amount, product: { id: productId } }) => ({ productId, amount })),
        });
    },
});
