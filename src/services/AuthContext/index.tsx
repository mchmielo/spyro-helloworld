import React, { createContext, PropsWithChildren, useContext, useEffect, useMemo, useState } from 'react';
import { firebaseService } from 'services/firebase';

export type User = firebase.User | null;

interface AuthContextProps {
    readonly user?: User;
}

interface AuthContextType {
    readonly user: User;
}

const AuthContext = createContext<AuthContextType>({
    user: null,
});

export function useAuthContext() {
    return useContext(AuthContext);
}

export function AuthContextProvider({ children }: PropsWithChildren<AuthContextProps>) {
    const [user, setUser] = useState<User>(null);

    useEffect(() => {
        firebaseService.getRedirectResult().then(({ user }) => setUser(user));

        firebaseService.onAuthStateChanged(setUser);
    }, []);

    const value = useMemo<AuthContextType>(() => ({ user }), [user]);

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}
