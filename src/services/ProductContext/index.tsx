import { noop, cond, constant, matches, find } from 'lodash';
import React, { createContext, useCallback, useContext, useMemo, useReducer, useRef } from 'react';
import { Product, ProductId } from 'services/interfaces';
import * as productsService from 'services/products';
import firebase from 'firebase';

const INITIAL_STATE: ProductState = {
    products: [],
    isLoading: false,
};

const SET_PRODUCTS_ACTION = 'SET_PRODUCTS';
const SET_IS_LOADING_ACTION = 'SET_IS_LOADING';

type ProductSetAction = { readonly type: 'SET_PRODUCTS'; readonly products: Product[] };
type IsLoadingSetAction = { readonly type: 'SET_IS_LOADING'; readonly isLoading: boolean };

interface ProductState {
    readonly products: Product[];
    readonly isLoading: boolean;
}

interface ProductType extends ProductState {
    fetchProducts(): void;
    getProduct(id: ProductId): Promise<Product>;
}

export function useProducts() {
    const temp = useContext(ProductContext);
    return temp;
}

export const ProductContext = createContext<ProductType>({
    ...INITIAL_STATE,
    fetchProducts: noop,
    getProduct: (id: ProductId) => Promise.reject(),
});

export function ProductContextProvider({ children }: React.PropsWithChildren<Record<string, any>>) {
    const [productState, dispatch] = useReducer(productReducer, INITIAL_STATE);
    const productSnapshot = useRef<firebase.firestore.DocumentData | undefined | null>(null);
    const fetchProducts = useCallback(() => {
        dispatch(setIsLoading(true));
        productsService
            .getProducts(productSnapshot.current, 10)
            .then(([snapshot, productsFromFirebase]) => {
                productSnapshot.current = snapshot;
                dispatch(setProducts([...(productState.products || []), ...productsFromFirebase]));
            })
            .finally(() => {
                dispatch(setIsLoading(false));
            });
    }, [dispatch, productState.products]);

    const getProduct = useCallback(
        (id: ProductId) => {
            const product = find(productState.products, { id: id });
            return product ? Promise.resolve(product) : Promise.reject();
        },
        [productState.products],
    );

    const contextValue = useMemo(() => ({ ...productState, fetchProducts, getProduct }), [
        productState,
        fetchProducts,
        getProduct,
    ]);

    return <ProductContext.Provider value={contextValue}>{children}</ProductContext.Provider>;
}

function productReducer(state: ProductState, action: any): ProductState {
    return cond([
        [matches(SET_PRODUCTS_ACTION), () => ({ ...state, products: (action as ProductSetAction).products })],
        [matches(SET_IS_LOADING_ACTION), () => ({ ...state, isLoading: (action as IsLoadingSetAction).isLoading })],
        [() => true, constant(state)],
    ])(action.type);
}

function setProducts(products: Product[]): ProductSetAction {
    return {
        type: SET_PRODUCTS_ACTION,
        products,
    };
}

function setIsLoading(isLoading: boolean): IsLoadingSetAction {
    return {
        type: SET_IS_LOADING_ACTION,
        isLoading: isLoading,
    };
}
