import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import { FirebaseOrder, FirebaseProduct } from 'services';

const firebaseConfig = {
    apiKey: 'AIzaSyCG4kbIF83s_5CqQ0zhxk33RmGCvktorpc',
    authDomain: 'spyro-hello-world.firebaseapp.com',
    databaseURL: 'https://spyro-hello-world.firebaseio.com',
    projectId: 'spyro-hello-world',
    storageBucket: 'spyro-hello-world.appspot.com',
    messagingSenderId: '140264021395',
    appId: '1:140264021395:web:d62bebde801999b2365c66',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const firebaseAuth = firebaseApp.auth();

const firestore = firebaseApp.firestore();

export const collections = Object.freeze({
    products: firestore.collection('products') as firebase.firestore.CollectionReference<FirebaseProduct>,
    orders: firestore.collection('orders') as firebase.firestore.CollectionReference<FirebaseOrder>,
});

export const firebaseService = Object.freeze({
    async signInWithGoogle() {
        await firebaseAuth.useDeviceLanguage();
        await firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        return await firebaseAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
    },
    async getRedirectResult() {
        return await firebaseAuth.getRedirectResult();
    },
    async signOut() {
        return await firebaseAuth.signOut();
    },
    onAuthStateChanged: firebaseAuth.onAuthStateChanged.bind(firebaseAuth),
});
