import { cond, constant, find, matches, noop } from 'lodash';
import React, { createContext, useCallback, useContext, useMemo, useReducer } from 'react';
import { Address, Product, ProductId } from 'services/interfaces';
import { useProducts } from 'services/ProductContext';

type CartMap = Map<ProductId, number>;

interface CartState {
    readonly cart: CartMap;
    readonly orderAddress?: Address;
}

type AddToCartAction = { type: 'ADD_TO_CART'; productId: ProductId };
type RemoveFromCartAction = { type: 'REMOVE_FROM_CART'; productId: ProductId };
type IncrementAction = { type: 'INCREMENT'; productId: ProductId };
type DecrementAction = { type: 'DECREMENT'; productId: ProductId };
type SetAmountAction = { type: 'SET_AMOUNT'; productId: ProductId; amount: number };
type SetOrderAddressAction = { type: 'SET_ORDER_ADDRESS'; address: Address };
type ClearCartAction = { type: 'CLEAR_CART' };

type CartAction =
    | AddToCartAction
    | RemoveFromCartAction
    | IncrementAction
    | DecrementAction
    | SetAmountAction
    | SetOrderAddressAction
    | ClearCartAction;

type CartContextType = {
    readonly cart: Array<{ product: Product; amount: number }>;
    readonly orderAddress?: Address;
    addToCart(id: ProductId): void;
    removeFromCart(id: ProductId): void;
    isInCart(id: ProductId): boolean;
    increment(id: ProductId): void;
    decrement(id: ProductId): void;
    setAmount(id: ProductId, amount: number): void;
    setOrderAddress(address: Address): void;
    clearCart(): void;
};

const CartContext = createContext<CartContextType>({
    cart: [],
    orderAddress: undefined,
    addToCart: noop,
    removeFromCart: noop,
    isInCart: constant(false),
    increment: noop,
    decrement: noop,
    setAmount: noop,
    setOrderAddress: noop,
    clearCart: noop,
});

export function useCartContext() {
    return useContext(CartContext);
}

export function CartContextProvider({ children }: React.PropsWithChildren<Record<string, any>>) {
    const [{ cart, orderAddress }, dispatch] = useReducer(cartReducer, {
        cart: new Map<ProductId, number>(),
        orderAddress: undefined,
    });

    const products = useProducts();

    const addToCart = useCallback(
        (id: ProductId) => {
            dispatch(addToCartAction(id));
        },
        [dispatch],
    );

    const removeFromCart = useCallback(
        (id: ProductId) => {
            dispatch(removeFromCartAction(id));
        },
        [dispatch],
    );

    const isInCart = useCallback((id: ProductId) => cart.has(id), [cart]);

    const increment = useCallback(
        (id: ProductId) => {
            dispatch(incrementAction(id));
        },
        [dispatch],
    );

    const decrement = useCallback(
        (id: ProductId) => {
            dispatch(decrementAction(id));
        },
        [dispatch],
    );

    const setAmount = useCallback(
        (id: ProductId, amount: number) => {
            dispatch(setAmountAction(id, amount));
        },
        [dispatch],
    );

    const setOrderAddress = useCallback(
        (address: Address) => {
            dispatch(setOrderAddressAction(address));
        },
        [dispatch],
    );

    const clearCart = useCallback(() => {
        dispatch(clearCartAction());
    }, [dispatch]);

    const contextValue: CartContextType = useMemo(
        () => ({
            cart: [...cart.keys()]
                .map((productId) => ({
                    product: find(products.products, { id: productId }) as Product,
                    amount: cart.get(productId) || 0,
                }))
                .filter(({ product }) => Boolean(product)),
            orderAddress,
            addToCart,
            removeFromCart,
            isInCart,
            increment,
            decrement,
            setAmount,
            setOrderAddress,
            clearCart,
        }),
        [
            products,
            cart,
            addToCart,
            removeFromCart,
            isInCart,
            increment,
            decrement,
            setAmount,
            setOrderAddress,
            orderAddress,
            clearCart,
        ],
    );

    return <CartContext.Provider value={contextValue}>{children}</CartContext.Provider>;
}

function cartReducer({ cart, orderAddress }: CartState, action: CartAction): CartState {
    return cond([
        [
            matches('ADD_TO_CART'),
            () => {
                const productId = (action as AddToCartAction).productId;
                return {
                    orderAddress,
                    cart: cart.has(productId) ? cart : new Map<ProductId, number>(cart).set(productId, 1),
                };
            },
        ],
        [
            matches('REMOVE_FROM_CART'),
            () => {
                const productId = (action as RemoveFromCartAction).productId;
                let newCart = cart;
                if (cart.has(productId)) {
                    newCart = new Map<ProductId, number>(cart);
                    newCart.delete(productId);
                }
                return { orderAddress, cart: newCart };
            },
        ],
        [
            matches('INCREMENT'),
            () => {
                const productId = (action as IncrementAction).productId;
                return {
                    orderAddress,
                    cart: cart.has(productId)
                        ? new Map<ProductId, number>(cart).set(productId, (cart.get(productId) || 0) + 1)
                        : cart,
                };
            },
        ],
        [
            matches('DECREMENT'),
            () => {
                const productId = (action as DecrementAction).productId;
                return {
                    orderAddress,
                    cart: cart.has(productId)
                        ? new Map<ProductId, number>(cart).set(productId, Math.max((cart.get(productId) || 0) - 1, 0))
                        : cart,
                };
            },
        ],
        [
            matches('SET_AMOUNT'),
            () => {
                const { productId, amount } = action as SetAmountAction;
                return { orderAddress, cart: cart.has(productId) ? cart.set(productId, amount) : cart };
            },
        ],
        [
            matches('SET_ORDER_ADDRESS'),
            () => {
                return { orderAddress: (action as SetOrderAddressAction).address, cart };
            },
        ],
        [
            matches('CLEAR_CART'),
            () => {
                return {
                    orderAddress: undefined,
                    cart: new Map<ProductId, number>(),
                };
            },
        ],
        [() => true, constant({ cart, orderAddress })],
    ])(action.type);
}

function addToCartAction(productId: ProductId): AddToCartAction {
    return { type: 'ADD_TO_CART', productId };
}

function removeFromCartAction(productId: ProductId): RemoveFromCartAction {
    return { type: 'REMOVE_FROM_CART', productId };
}

function incrementAction(productId: ProductId): IncrementAction {
    return { type: 'INCREMENT', productId };
}

function decrementAction(productId: ProductId): DecrementAction {
    return { type: 'DECREMENT', productId };
}

function setAmountAction(productId: ProductId, amount: number): SetAmountAction {
    return { type: 'SET_AMOUNT', productId, amount };
}

function setOrderAddressAction(address: Address): SetOrderAddressAction {
    return { type: 'SET_ORDER_ADDRESS', address };
}

function clearCartAction(): ClearCartAction {
    return { type: 'CLEAR_CART' };
}
